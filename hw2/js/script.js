"use strict";

const root = document.querySelector("#root");
const booksList = document.createElement("ul");
const books = [
  {
    author: "Скотт Бэккер",
    name: "Тьма, что приходит прежде",
    price: 70,
  },
  {
    author: "Скотт Бэккер",
    name: "Воин-пророк",
  },
  {
    name: "Тысячекратная мысль",
    price: 70,
  },
  {
    author: "Скотт Бэккер",
    name: "Нечестивый Консульт",
    price: 70,
  },
  {
    author: "Дарья Донцова",
    name: "Детектив на диете",
    price: 40,
  },
  {
    author: "Дарья Донцова",
    name: "Дед Снегур и Морозочка",
  },
];

books.forEach((element, index) => {
  try {
    if (!element.hasOwnProperty("author")) {
      throw new Error(`Нет автора в ${index + 1}й книге`);
    }
    if (!element.hasOwnProperty("name")) {
      throw new Error(`Нет названия в ${index + 1}й книге`);
    }
    if (!element.hasOwnProperty("price")) {
      throw new Error(`Нет цены в ${index + 1}й книге`);
    }

    // const titles = Object.keys(element);
    // ["author", "name", "price"].forEach((element) => {
    //   if (!titles.includes(element)) {
    //     throw new Error(`Нет ${element} в ${index + 1}й книге`);
    //   }
    // });

    const { author, name, price } = element;

    const li = document.createElement("li");
    const authorParagraph = document.createElement("p");
    const nameParagraph = document.createElement("p");
    const priceParagraph = document.createElement("p");

    authorParagraph.textContent = `Автор: ${author}`;
    nameParagraph.textContent = `Название: ${name}`;
    priceParagraph.textContent = `Цена: ${price}`;

    li.append(authorParagraph, nameParagraph, priceParagraph);

    booksList.append(li);
  } catch (error) {
    console.log(error);
  }
});

root.append(booksList);
