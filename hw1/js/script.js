// Обьясните своими словами, как вы понимаете, как работает прототипное наследование в Javascript

// Прототипное наследование дает возможность получить свойства и методы одного объекта от другого.
// Допустим есть обьект user {...} и есть uasya {...},
// если нам надо чтоб uasya что-то умел что умеет user мы делаем его прототипом от user.
// Теперь все свойства и методы что есть у user есть и у обьекта uasya
// Как-то так )

'use strict';

import { Employee } from "./classes/Employee.js";
import { Programmer } from "./classes/Programmer.js";

const employee1 = new Employee({
	name: 'Nick',
	age: 37,
	salary: 700,
});

const employee2 = new Programmer({
	name: 'Ualia',
	age: 47,
	salary: 1500,
	lang: ['C++', 'C#', 'Java'],
});

const employee3 = new Programmer({
	name: 'Uasya',
	age: 27,
	salary: 2500,
	lang: ['NodeJs', 'JavaScript', 'Java'],
});

console.log(employee1);
console.log(employee2);
console.log(employee3);