export class Employee {
	constructor(props) {
		this._name = props.name;
		this._age = props.age;
		this._salary = props.salary;
	}

	get name() {
		return this._name;
	}
	set name(value) {
		this._name = value;
	}

	get age() {
		return this._age;
	}
	set age(value) {
		this._age = value;
	}

	get salary() {
		return this._salary;
	}
	set salary(value) {
		this._salary = value;
	}
}