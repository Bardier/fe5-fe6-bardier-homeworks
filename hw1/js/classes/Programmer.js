import { Employee } from './Employee.js';

export class Programmer extends Employee {
	constructor(props) {
		super(props);

		this._lang = props.lang;
	}

	get lang() {
		return this._lang;
	}
	set lang(value) {
		this._lang = value;
	}

	get salary() {
		return this._salary * 3;
	}

}