"use strict";

// function User(name, age) {
//     this.name = name;
//     this.age = age;
// }

// const user = new User("Uasya", 18);

// console.log(user.__proto__);
// console.log(User.prototype);
// console.log(User.prototype === user.__proto__);

// User.prototype.getName = function () {
//     return this.name;
// };

// console.log(user.getName());

// function Us() {}
// Us.prototype = {
//     name: "Uasyliy",
//     getName: function () {
//         return this.name;
//     },
// };
// const us = new Us();
// console.log(us);
// console.log(us.name);

// function foo() {}
// console.log(foo);
// let bar = { a: "a4" };

// foo.prototype = bar;
// const baz = Object.create(bar);
// console.log(baz instanceof foo);
// console.log(bar instanceof foo);
// console.log(baz.__proto__ === foo.prototype);

// const faz = new foo();
// console.log(faz instanceof foo);
// console.log(faz);
// console.log(faz.a);

// console.log(faz.__proto__ === baz.__proto__);

// class User {
//     constructor(name, age) {
//         this.name = name;
//         this.age = age;
//         this.login = this._getLogin();
//     }

//     set age(value) {
//         this._age = value;
//     }
//     get age() {
//         return this._age;
//     }

//     static getAge(obj) {
//         return obj.age;
//     }

//     _getLogin() {
//         return this.name.toLowerCase() + this.age;
//     }

//     someMethod() {
//         console.log(`User method`);
//         return null;
//         console.log("12345");
//     }
// }

// const user = new User("Uasya", 21);
// console.log(user.age);
// console.log(User.getAge(user));

// console.log(Object.hasOwn(user, "name"));
// user.someMethod();

// class Admin extends User {
//     constructor(name, age, sallary) {
//         super(name, age);
//         this.sallary = sallary;
//     }

//     someMethod() {
//         console.log(`start admin method`);
//         super.someMethod();
//         console.log(`Admin method`);
//     }
// }

// const admin = new Admin("Arkadiy", 19, 100500);
// // console.log(admin);

// admin.someMethod();

// console.log(admin instanceof Admin);
// console.log(admin instanceof User);

// console.log(window.self);

// TASK 1
// class Patient {
//     constructor(props) {
//         this.name = props.name;
//         this.birthDate = props.birthDate;
//         this.sex = props.sex;
//     }
// }

// const patient = new Patient({ name: "Magomed", birthDate: 1987, sex: "male" });
// console.log(patient);

// class HeartPatient extends Patient {
//     constructor(props) {
//         super(props);
//         this.avgBloodPressure = props.avgBloodPressure;
//         this.heartProblems = props.heartProblems;
//     }
// }

// const heartPatient = new HeartPatient({
//     name: "Magomed",
//     birthDate: 1987,
//     sex: "male",
//     avgBloodPressure: "12080",
//     heartProblems: "open heart surgery",
// });
// console.log(heartPatient);

// class dentistPatient extends Patient {
//     constructor(props) {
//         super(props);
//         this.lastVisitDate = props.lastVisitDate;
//         this.currentTreatment = props.currentTreatment;
//     }
// }

// TASK 2
/* <div id="idОкна" class="классыОкна">
      <div class="modal-content">
      <span class="close">&times;</span>
      <p>ТекстОкна</p>
      </div>
      </div> */

// class Modal {
//     constructor(id, classes, text) {
//         this.id = id;
//         this.classes = classes;
//         this.text = text;
//     }
//     render() {
//         const mainDiv = document.createElement("div");
//         const contentDiv = document.createElement("div");
//         const closeSpan = document.createElement("span");
//         const textContent = document.createElement("p");
//         mainDiv.append(contentDiv);
//         contentDiv.append(closeSpan);
//         contentDiv.append(textContent);

//         mainDiv.setAttribute("id", this.id);
//         mainDiv.classList.add(...this.classes);
//         contentDiv.classList.add("modal-content");
//         closeSpan.classList.add("close");
//         closeSpan.innerHTML = "&times;";

//         closeSpan.addEventListener("click", this.closeModal);

//         textContent.textContent = this.text;
//         this.mainDiv = mainDiv;
//         return this.mainDiv;
//     }
//     openModal() {
//         this.mainDiv.classList.add("active");
//     }
//     closeModal = () => {
//         this.mainDiv.classList.remove("active");
//     };
// }

// const myModal = new Modal("firsrModal", ["modal", "myClass"], "myText");
// const root = document.getElementById("root");
// const closeBtn = document.getElementById("myBtn");
// root.append(myModal.render());
// closeBtn.addEventListener("click", function () {
//     myModal.openModal();
// });

// TASK 3

class Input {
    constructor(props) {
        this.type = props.type;
        this.name = props.name;
        this.isRequired = props.isRequired;
        this.id = props.id;
        this.classes = props.classes;
        this.placeholder = props.placeholder;
        this.errorText = props.errorText;
        this.value = props.value;
    }

    render() {
        const input = document.createElement("input");

        if (this.type) {
            input.type = this.type;
        }

        if (this.name) {
            input.name = this.name;
        }

        if (this.isRequired) {
            input.required = this.isRequired;
        }

        if (this.classes && this.classes.lenght > 0) {
            input.classList.add(...this.classes);
        }

        if (this.placeholder) {
            input.placeholder = this.placeholder;
        }

        if (this.value) {
            input.value = this.value;
        }

        if (this.id) {
            input.id = this.id;
        }
        input.addEventListener("blur", this.handleBlur.bind(this));
        return input;
    }

    handleBlur(e) {
        if (e.target.required && e.target.value.trim() === "") {
            alert(this.errorText);
        }
    }
}

class Form {
    constructor(id, classes, action = "") {
        this.id = id;
        this.classes = classes;
        this.action = action;
    }

    render() {
        this.form = document.createElement("form");
        this.form.setAttribute("id", this.id);
        this.form.classList.add(...this.classes);
        this.form.setAttribute("action", this.action);
        this.form.addEventListener("submit", this.handleSumbit.bind(this));
        return this.form;
    }
    handleSumbit(e) {
        e.preventDefault();

        let elems = [...e.target.elements].filter((e) => e.type !== "submit");

        console.log(elems);
        const serialize = this.serialize(elems);
        const serializeJSON = this.serializeJSON(elems);

        console.log(serialize);
        console.log(serializeJSON);
    }
    serialize(elems) {
        let fields = elems.reduce(
            (res, e) => res + e.name + "=" + e.value + "&",
            ""
        );

        return fields.slice(0, fields.length - 1);
    }
    serializeJSON(elems) {
        let fieldObj = {};
        elems.forEach((element) => {
            fieldObj[element.name] = element.value;
        });

        return fieldObj;
    }
}

class RegisterForm extends Form {
    constructor(id, classes, action = "") {
        super(id, classes, (action = ""));
    }

    render() {
        super.render();
        const loginInput = new Input({
            type: "text",
            name: "login",
            isRequired: true,
            id: "login",
            placeholder: "Ваш логин",
        }).render();

        const passwordInput = new Input({
            type: "password",
            name: "password",
            isRequired: true,
            id: "password",
            placeholder: "Ваш пароль",
        }).render();

        const submit = new Input({
            type: "submit",
            id: "password",
            value: "log in",
        }).render();

        const emailInput = new Input({
            type: "email",
            name: "email",
            isRequired: true,
            placeholder: "Ваш email",
        }).render();

        const repeatPasswordInput = new Input({
            type: "password",
            name: "repeat-password",
            isRequired: true,
            placeholder: "Повторите пароль",
        }).render();

        this.form.append(
            loginInput,
            emailInput,
            passwordInput,
            repeatPasswordInput,
            submit
        );
        return this.form;
    }
}

class LoginForm extends Form {
    constructor(id, classes, action = "") {
        super(id, classes, (action = ""));
    }
    render() {
        super.render();
        const loginInput = new Input({
            type: "text",
            name: "login",
            isRequired: true,
            id: "login",
            placeholder: "Ваш логин",
            errorText: "Введите логин",
        }).render();

        const passwordInput = new Input({
            type: "password",
            name: "password",
            isRequired: true,
            id: "password",
            placeholder: "Ваш пароль",
        }).render();

        const submit = new Input({
            type: "submit",
            id: "password",
            value: "log in",
        }).render();

        this.form.append(loginInput, passwordInput, submit);
        return this.form;
    }
}

class Modal {
    constructor(id, classes) {
        this.id = id;
        this.classes = classes;
    }
    render() {
        const mainDiv = document.createElement("div");
        const contentDiv = document.createElement("div");
        const closeSpan = document.createElement("span");
        mainDiv.append(contentDiv);
        contentDiv.append(closeSpan);

        mainDiv.setAttribute("id", this.id);
        mainDiv.classList.add(...this.classes);
        contentDiv.classList.add("modal-content");
        closeSpan.classList.add("close");
        closeSpan.innerHTML = "&times;";

        closeSpan.addEventListener("click", this.closeModal);

        this.mainDiv = mainDiv;
        return this.mainDiv;
    }
    openModal() {
        this.mainDiv.classList.add("active");
    }
    closeModal = () => {
        this.mainDiv.classList.remove("active");
    };
}

class Register extends Modal {
    constructor(id, classes) {
        super(id, classes);
    }

    render() {
        super.render();
        const form = new RegisterForm("registerForm", []).render();

        this.mainDiv.children[0].append(form);
        return this.mainDiv;
    }
}

class Auth extends Modal {
    constructor(id, classes) {
        super(id, classes);
    }

    render() {
        super.render();
        const form = new LoginForm("authForm", ["class1", "class2"]).render();

        this.mainDiv.children[0].append(form);
        return this.mainDiv;
    }
}

const root = document.getElementById("root");
const closeBtn = document.getElementById("myBtn");

const authBtn = document.getElementById("auth");
const registerBtn = document.getElementById("register");

const auth = new Auth("firsrModal", ["modal", "myClass"]);
root.append(auth.render());

const register = new Register("firsrModal", ["modal", "myClass"]);
root.append(register.render());

authBtn.addEventListener("click", function () {
    auth.openModal();
});

registerBtn.addEventListener("click", function () {
    register.openModal();
});
