import { ModalApp } from "./ModalApp.js";
import { Form } from "./Form.js";
import { Input } from "./Input.js";

export class ModalRegister extends ModalApp {
	constructor(props) {
		super(props);

		this._formAction = props.formAction;
	}

	get formAction() {
		return this._formAction;
	}
	set formAction(value) {
		this._formAction = value;
	}

	render() {
		super.render();

		const form = new Form({
			id: 'register-form',
			action: this.formAction,
		}).render();

		const login = new Input({
			type: 'text',
			name: 'login',
			placeholder: 'Ваш логин',
			required: true,
			errorText: 'Введите ваш логин',
		}).render();

		const email = new Input({
			type: 'email',
			name: 'email',
			placeholder: 'Ваш email',
			required: true,
			errorText: 'Введите ваш email',
		}).render();

		const password = new Input({
			type: 'password',
			name: 'password',
			placeholder: 'Ваш пароль',
			required: true,
			errorText: 'Введите ваш пароль',
		}).render();

		const passwordRepeat = new Input({
			type: 'password',
			name: 'password',
			placeholder: 'Повторите пароль',
			required: true,
			errorText: 'Повторите ваш пароль',
		}).render();

		const formSubmit = new Input({
			type: 'submit',
			value: 'Регистрация',
		}).render();

		form.append(login, email, password, passwordRepeat, formSubmit);
		this.modalWindowContent.append(form);

		return this.modalWindow;
	}
}
