export class ModalApp {

	constructor(params) {
		this._id = params.id;
		this._classes = params.classes;
	}

	get id() {
		return this._id;
	}
	set id(value) {
		this._id = value;
	}

	get classes() {
		return this._classes;
	}
	set classes(value) {
		this._classes = value;
	}

	render() {
		const modalWindow = document.createElement('div');
		const modalWindowContent = document.createElement('div');
		const modalWindowCloseBtn = document.createElement('span');

		modalWindow.setAttribute('id', this.id);
		modalWindow.classList.add(...this.classes);
		modalWindow.append(modalWindowContent);

		modalWindowContent.classList.add('modal-content');
		modalWindowContent.append(modalWindowCloseBtn);

		modalWindowCloseBtn.classList.add('close');
		modalWindowCloseBtn.innerHTML = '&times;';

		this.modalWindow = modalWindow;
		this.modalWindowContent = modalWindowContent;
	}

	openModal() {
		this.modalWindow.classList.add('active');
	}

	closeModal() {
		this.modalWindow.classList.remove('active');
	}
}