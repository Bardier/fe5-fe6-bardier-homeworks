import { ModalApp } from "./ModalApp.js";
import { Form } from "./Form.js";
import { Input } from "./Input.js";

export class ModalAuthorization extends ModalApp {
	constructor(props) {
		super(props);

		this._formAction = props.formAction;
	}

	get formAction() {
		return this._formAction;
	}
	set formAction(value) {
		this._formAction = value;
	}

	render() {
		super.render();

		const form = new Form({
			id: 'auth-form',
			action: this.formAction,
		}).render();

		const login = new Input({
			type: 'text',
			name: 'login',
			placeholder: 'Ваш логин или email',
			required: true,
		}).render();

		const password = new Input({
			type: 'password',
			name: 'password',
			placeholder: 'Ваш пароль',
			required: true,
		}).render();

		const formSubmit = new Input({
			type: 'submit',
			value: 'Вход',
		}).render();

		form.append(login, password, formSubmit);
		this.modalWindowContent.append(form);

		return this.modalWindow;
	}
}