import { ModalApp } from "./ModalApp.js";

export class ModalWindow extends ModalApp {
	constructor(params) {
		super(params);

		this._text = params.text;
	}

	get text() {
		return this._text;
	}
	set text(value) {
		this._text = value;
	}

	render() {
		super.render();

		const modalWindowText = document.createElement('p');

		modalWindowText.textContent = this.text;

		this.modalWindowContent.append(modalWindowText);

		return this.modalWindow;
	}
}