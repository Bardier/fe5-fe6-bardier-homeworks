export class Input {
	constructor(props) {
		this._type = props.type;
		this._name = props.name;
		this._id = props.id;
		this._classes = props.classes;
		this._placeholder = props.placeholder;
		this._value = props.value;
		this._required = props.required;
		this._errorText = props.errorText;
	}

	get type() {
		return this._type;
	}
	set type(value) {
		this._type = value;
	}

	get name() {
		return this._name;
	}
	set name(value) {
		this._name = value;
	}

	get id() {
		return this._id;
	}
	set id(value) {
		this._id = value;
	}

	get classes() {
		return this._classes;
	}
	set classes(value) {
		this._classes = value;
	}

	get placeholder() {
		return this._placeholder;
	}
	set placeholder(value) {
		this._placeholder = value;
	}

	get value() {
		return this._value;
	}
	set value(value) {
		this._value = value;
	}

	get required() {
		return this._required;
	}
	set required(value) {
		this._required = value;
	}

	get errorText() {
		return this._errorText;
	}
	set errorText(value) {
		this._errorText = value;
	}


	render() {
		const input = document.createElement('input');

		if (this.type) input.setAttribute('type', this.type);
		if (this.name) input.setAttribute('name', this.name);
		if (this.id) input.setAttribute('id', this.id);
		if (this.classes) {
			this.classes.forEach(el => {
				input.classList.add(el);
			});
		}
		if (this.placeholder) input.setAttribute('placeholder', this.placeholder);
		if (this.value) input.setAttribute('value', this.value);
		if (this.required) input.required = true;

		this.input = input;

		this.handleBlur();

		return input;
	}

	handleBlur() {
		this.input.addEventListener('blur', () => {
			if (!this.input.value.trim() && this.input.required) {
				alert(this.errorText);
			}
		});
	}
}