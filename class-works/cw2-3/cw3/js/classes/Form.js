export class Form {
	constructor(props) {
		this._id = props.id;
		this._classes = props.classes;
		this._action = props.action;
	}

	get id() {
		return this._id;
	}
	set id(value) {
		this._id = value;
	}

	get classes() {
		return this._classes;
	}
	set classes(value) {
		this._classes = value;
	}

	get action() {
		return this._action;
	}
	set action(value) {
		this._action = value;
	}

	render() {
		this.form = document.createElement('form');

		if (this.id) this.form.setAttribute('id', this.id);
		if (this.classes) {
			this.classes.forEach(el => {
				this.form.classList.add(el);
			});
		}
		if (this.action) this.form.setAttribute('action', this.action);

		this.form.addEventListener('submit', this.handleSumbit.bind(this));

		return this.form;

	}


	handleSumbit(e) {
		e.preventDefault();

		const inputs = [...e.target.elements].filter(e => e.type !== 'submit');

		this.serialize(inputs);
	}

	serialize(inputs) {

		let res = '';
		inputs.forEach(el => {
			// todo Доделать вывод атрибуто = значений

		});

		let fields = elems.reduce(
			(res, e) => res + e.name + "=" + e.value + "&",
			""
		);

		console.log(res);
	}

}