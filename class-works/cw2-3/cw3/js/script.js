import { ModalWindow } from "./classes/ModalWindow.js";
import { ModalRegister } from "./classes/ModalRegister.js";
import { ModalAuthorization } from "./classes/ModalAuthorization.js";

// * Общие переменные (root и кнопки открытия форм)
const root = document.querySelector('#root');
const modalWindowBtnOpen = document.querySelector('#myBtn');
const modalRegisterBtnOpen = document.querySelector('#btn-register');
const modalAuthorizationBtnOpen = document.querySelector('#btn-enter');


// * Объекты классов
const modalWindow = new ModalWindow({
	id: 'modal-window',
	classes: [
		'modal',
	],
	text: 'Модальное окно',
});

const modalRegister = new ModalRegister({
	id: 'modal-register',
	classes: [
		'modal',
	],
	formAction: 'www.register',
});

const modalAuthorization = new ModalAuthorization({
	id: 'modal-authorization',
	classes: [
		'modal',
	],
	formAction: 'www.authorization',
});


// * render форм в DOM дереве
root.append(modalWindow.render());
root.append(modalRegister.render());
root.append(modalAuthorization.render());


// * Кнопки закрытия форм и прослушки на открытие-закрытие форм
const modalWindowBtnClose = document.querySelector('#modal-window .close');
modalWindowBtnOpen.addEventListener('click', modalWindow.openModal.bind(modalWindow));
modalWindowBtnClose.addEventListener('click', modalWindow.closeModal.bind(modalWindow));

const modalRegisterBtnClose = document.querySelector('#modal-register .close');
modalRegisterBtnOpen.addEventListener('click', modalRegister.openModal.bind(modalRegister));
modalRegisterBtnClose.addEventListener('click', modalRegister.closeModal.bind(modalRegister));

const modalAuthorizationBtnClose = document.querySelector('#modal-authorization .close');
modalAuthorizationBtnOpen.addEventListener('click', modalAuthorization.openModal.bind(modalAuthorization));
modalAuthorizationBtnClose.addEventListener('click', modalAuthorization.closeModal.bind(modalAuthorization));
