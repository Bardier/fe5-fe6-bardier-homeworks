
import { Pacient } from "./classes/Pacient.js";
import { PatientCardiologist } from "./classes/PatientCardiologist.js";
import { PatientStomatologist } from "./classes/PatientStomatologist.js";

const pacient1 = new Pacient({
	name: 'Nick',
	birthday: '12.09.1984',
	sex: 'male',
});
const pacient2 = new PatientCardiologist({
	name: 'Uasua',
	birthday: '13.01.1987',
	sex: 'male',
	averagePressure: 125,
	transferredProblems: 'Много нервничал'
});
const pacient3 = new PatientStomatologist({
	name: 'Nastya',
	birthday: '11.08.1994',
	sex: 'female',
	dateOfLastVisit: '04.09.2022',
	currentTreatment: 'Полоскать рот, чистить зубы'
});

console.log(pacient1);
console.log(pacient2);
console.log(pacient3);
