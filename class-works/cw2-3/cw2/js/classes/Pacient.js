export class Pacient {
	constructor(params) {
		this._name = params.name;
		this._birthday = params.birthday;
		this._sex = params.sex;
	}

	get name() {
		return this._name;
	}
	set name(value) {
		this._name = value;
	}

	get birthday() {
		return this._birthday;
	}
	set birthday(value) {
		this._birthday = value;
	}

	get sex() {
		return this._sex;
	}
	set sex(value) {
		this._sex = value;
	}
}