import { Pacient } from "./Pacient.js";

export class PatientCardiologist extends Pacient {
	constructor(params) {
		super(params);

		this._averagePressure = params.averagePressure;
		this._transferredProblems = params.transferredProblems;
	}

	get averagePressure() {
		return this._averagePressure;
	}
	set averagePressure(value) {
		this._averagePressure = value;
	}

	get transferredProblems() {
		return this._transferredProblems;
	}
	set transferredProblems(value) {
		this._transferredProblems = value;
	}
}