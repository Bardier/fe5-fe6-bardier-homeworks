import { Pacient } from "./Pacient.js";

export class PatientStomatologist extends Pacient {
	constructor(params) {
		super(params);

		this._dateOfLastVisit = params.dateOfLastVisit;
		this._currentTreatment = params.currentTreatment;
	}

	get dateOfLastVisit() {
		return this._dateOfLastVisit;
	}
	set dateOfLastVisit(value) {
		this._dateOfLastVisit = value;
	}

	get currentTreatment() {
		return this._currentTreatment;
	}
	set currentTreatment(value) {
		this._currentTreatment = value;
	}
}